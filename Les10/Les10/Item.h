#pragma once
#include<iostream>
#include<string>
#include<algorithm>
#include <exception>

class PriceException : public std::exception
{
	const char* what() const throw ()
	{
		return "Price can't be 0 or less.;";
	}
};

using std::string;

class Item
{
public:
	Item(string, string, double);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//get and set functions

	string getName() const { return this->_name; }
	string getSerialNumber() const { return this->_serialNumber; }

	int getCount() const { return this->_count; }

	double getUnitPrice() const { return this->_unitPrice; }

	void setName(string newName) { this->_name = newName; }
	void setSerialNumber(string newSerialNumber) { this->_serialNumber = newSerialNumber; }
	void setUnitPrice(double newUnitPrice) { this->_unitPrice = newUnitPrice; }
	void setCount(int newCount) { this->_count = newCount; }

private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!
};