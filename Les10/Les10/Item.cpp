#include "Item.h"

Item::Item(string name, string serialNum, double price)
{
	this->_name = name;
	this->_serialNumber = serialNum;

	this->_count = 1;

	if (price > 0) this->_unitPrice = price;
	else throw PriceException();
}

Item::~Item() {}

double Item::totalPrice() const { return this->_count * this->_unitPrice; }

bool Item::operator < (const Item& other) const { return this->_unitPrice < other._unitPrice; }

bool Item::operator > (const Item& other) const { return this->_unitPrice > other._unitPrice; }

bool Item::operator == (const Item& other) const { return this->_unitPrice == other._unitPrice; }