#include "Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	double sum = 0;
	for (std::set<Item>::iterator itemItr = this->_items.begin();
		itemItr != this->_items.end(); ++itemItr) sum += itemItr->totalPrice();
	return sum;
}

void Customer::addItem(Item newItem)
{
	std::set<Item>::iterator it = this->_items.find(newItem);
	if (it != this->_items.end())
	{
		Item temp = newItem;
		newItem.setCount(newItem.getCount() + 1);
		this->_items.erase(temp);
	}
	this->_items.insert(newItem);
}

void Customer::removeItem(Item remove)
{
	std::set<Item>::iterator it = this->_items.find(remove);
	if (it != this->_items.end())
	{
		if (it->getCount() > 0)
		{
			Item temp = remove;
			remove.setCount(remove.getCount() - 1);
			this->_items.erase(temp);
			this->_items.insert(remove);
		}
		else this->_items.erase(remove);
	}
}