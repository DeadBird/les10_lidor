#pragma once
#include"Item.h"
#include<set>

using std::string;

class Customer
{
public:
	Customer() {}
	Customer(string name);
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item);//add item to the set
	void removeItem(Item);//remove item from the set

	//get and set functions

	string getName() { return this->_name; }

private:
	std::string _name;
	std::set<Item> _items;
};
