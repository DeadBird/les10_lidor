#include"Customer.h"
#include <iostream>
#include<map>

using namespace std;

#define ITEM_COUNT 10

#define ADD_CUSTOMER_OP string("1")
#define UPDATE_USER_OP string("2")
#define PRINT_MOST_PAYING_CUSTOMER_OP string("3")
#define EXIT_OP string("4")

void getMainUserChoice(string& userChoice)
{
	cout << "Welcome to MagshiMart!" << endl
		<< "1 - To sign as customer and buy items" << endl
		<< "2 - To uptade existing customer's items" << endl
		<< "3 - To print the customer who pays the most" << endl
		<< "4 - Exit" << endl << "input: ";

	cin >> userChoice;
}

void printItemList(Item itemList[], int listLen)
{
	for (int i = 0; i < listLen; i++) cout << i + 1 << " -> " << itemList[i].getName() << ": " << itemList[i].getUnitPrice() << endl;
	cout << "Enter 0 when you have everything!" << endl << "input: ";
}

void addItems(Customer* customer, Item itemList[], int listLen)
{
	int userChoice = -1;
	printItemList(itemList, listLen);
	cin >> userChoice;
	while (userChoice != 0)
	{
		userChoice--;
		if (userChoice >= 0 && userChoice < listLen) { customer->addItem(itemList[userChoice]); }
		else cout << "Unknown item" << endl;

		cout << endl;

		printItemList(itemList, listLen);
		cin >> userChoice;
	}
}

void removeItems(Customer* customer, Item itemList[], int listLen)
{
	int userChoice = -1;
	printItemList(itemList, listLen);
	cin >> userChoice;
	while (userChoice != 0)
	{
		userChoice--;
		if (userChoice >= 0 && userChoice < listLen) { customer->removeItem(itemList[userChoice]); }
		else cout << "Unknown item" << endl;

		cout << endl;

		printItemList(itemList, listLen);
		cin >> userChoice;
	}
}

void addCustomer(map<string, Customer>& customers, Item itemList[], int listLen)
{
	// Get new customer name
	string userName;
	cout << "What is your name?: ";
	cin >> userName;

	Customer newCustomer(userName);

	//Get customer items
	addItems(&newCustomer, itemList, listLen);

	customers.insert(pair<string, Customer>(userName, newCustomer));
}

void editCustomer(map<string, Customer>& customers, Item itemList[], int listLen)
{
	string userName;
	cout << "What is your name?: ";
	cin >> userName;

	Customer* p;

	if (customers.find(userName) != customers.end())
	{
		string option = "";
		p = &customers[userName];

		while (option != "3")
		{
			cout << "1 - Add items" << endl << "2 - Remove items" << endl << "3 - Back" << endl << "input: ";
			cin >> option;

			if (option == "1") addItems(p, itemList, listLen);
			else if (option == "2") removeItems(p, itemList, listLen);
			else cout << "Invalid option!" << endl;
		}
	}
}

void printMostPayingCustomer(map<std::string, Customer> customers)
{
	int max;
	map<std::string, Customer>::iterator curr = customers.begin(), maxPayer = curr;
	max = curr->second.totalSum();
	for (curr; curr != customers.end(); curr++)
	{
		if (max < curr->second.totalSum())
		{
			max = curr->second.totalSum();
			maxPayer = curr;
		}
	}
	cout << "Name of most paying customer is: " << maxPayer->second.getName() << endl << "He is paying: " << maxPayer->second.totalSum() << endl;
}

int main()
{
	map<std::string, Customer> abcCustomers;
	string userChoice;
	Item itemList[ITEM_COUNT] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };

	while (userChoice != EXIT_OP)
	{
		getMainUserChoice(userChoice);
		if (userChoice == ADD_CUSTOMER_OP) addCustomer(abcCustomers, itemList, ITEM_COUNT);
		else if (userChoice == UPDATE_USER_OP) editCustomer(abcCustomers, itemList, ITEM_COUNT);
		else if (userChoice == PRINT_MOST_PAYING_CUSTOMER_OP) printMostPayingCustomer(abcCustomers);
		else if (userChoice == EXIT_OP) cout << "Shuting down...";
		else cout << "Invalid option!" << endl;
	}

	return 0;
}